# tnt-dump

This repository contains some Python 3 scripts which can be used for various
purposes: mainly to extract information from TNTVillage's releaselist public
interface and archive all the torrent files hosted by the aforementioned
website.

The scripts have been coded and tested only on Linux.

## License

The scripts are licensed under the [modified ISC license](https://cvsweb.openbsd.org/src/share/misc/license.template?rev=HEAD)
(OpenBSD template). The copyright holder is the `tnt-phoenix` GitLab group
(open to the public). Please note that all the code contributed to this
repository will be licensed as such.

```
/*
 * Copyright (c) 2018 tnt-phoenix
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
```

## Usage

In this section I'll briefly provide usage information for each script
included in this repository.

### `HTML_pages_downloader.py`

The purpose of this script is to download all TNTVillage's releaselist's HTML
pages through Tor using cURL. Obviously it requires a working network
connection to the internet and a running Tor instance.

#### Linux software dependencies

- [cURL](https://curl.haxx.se/)
- [Tor](https://www.torproject.org/download/download-unix.html.en)

#### Python dependencies

- [beautifulsoup4](https://pypi.org/project/beautifulsoup4/)
- [lxml](https://pypi.org/project/lxml/)

#### Usage

`python3 /path/to/script/HTML_pages_downloader.py`

The HTML files will be downloaded to the current working directory.

### `HTML_to_CSV_parser.py`

The purpose of this script is to extract the relevant information from
TNTVillage's releaselist's HTML pages (previously downloaded using the
`HTML_pages_downloader.py` script or through other means) and write
those to a syntactically valid CSV file (";" separator).

#### Python dependencies

- [beautifulsoup4](https://pypi.org/project/beautifulsoup4/)
- [lxml](https://pypi.org/project/lxml/)

#### Usage

`python3 /path/to/script/HTML_to_CSV_parser.py`

The script assumes that all the HTML files (and only those) are available
in the current working directory. The resulting CSV file (named
`result.csv`) will be stored in the current working directory too.

### `torrent_files_downloader.py`

The purpose of this script is to download all TNTVillage's torrent
files listed in a user-provided text file (previously extracted,
for example from the `result.csv` file). Obviously it requires a working
network connection to the internet and a running Tor instance.

#### Linux software dependencies

- [cURL](https://curl.haxx.se/)
- [Tor](https://www.torproject.org/download/download-unix.html.en)

#### Usage

`python3 /path/to/script/torrent_files_downloader.py`

The script will read the URLs of the torrent files to download (one URL
for line) from a file named `url_list.txt` which must be available in the
current working directory. 

### `torrent_files_integrity_check.py`

The purpose of this script is to check if the torrent files read from the
current working directory are well formed and report the ones which are
not. It's useful to detect all the torrent files which were corrupted
(or are incomplete) due to network errors.

#### Python dependencies

- [libtorrent-rasterbar](https://www.libtorrent.org/python_binding.html)

#### Usage

`python3 /path/to/script/torrent_files_integrity_check.py`

The script assumes all the torrent files (and only those) are available
in the current working directory. If it produces no output it means no
errors have been detected.

### `rename_torrent_files.py`

The purpose of this script is to rename each torrent file available in
the current working directory using a filename based on its info_hash.

#### Python dependencies

- [libtorrent-rasterbar](https://www.libtorrent.org/python_binding.html)

#### Usage

`python3 /path/to/script/rename_torrent_files.py`

The script assumes all the torrent files (and only those) are available
in the current working directory. If will produce output only in case of
errors.

## Contributing

All kind of contributions are kindly welcomed, thanks!
