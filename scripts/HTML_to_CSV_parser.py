#!/usr/bin/env python3

import csv
import os
from bs4 import BeautifulSoup

CSV_content = []

for fn in os.listdir("."):
    with open(fn) as fp:
        SOUP = BeautifulSoup(fp, "lxml")

    HREFS = [link.get("href") for link in SOUP.find_all("a")]
    TDS = SOUP.findAll("td")[7:]  # Exclude table heading
    TEXTS = [TDS[i].get_text() for i in range(6, len(TDS), 7)]

    for i, title in enumerate(TEXTS):
        CSV_LINE = (title, HREFS[i * 4 + 2], HREFS[i * 4 + 3],
                    HREFS[i * 4], HREFS[i * 4 + 1])
        CSV_content.append(CSV_LINE)

CSV_HEADING = ("Titolo e sottotitolo release;URL Categoria;"
               "URL discussione forum TNT;URL download torrent;Magnet URI\n")

with open("result.csv", "w") as w:
    w.write(CSV_HEADING)
    WRITER = csv.writer(w, delimiter=";")
    WRITER.writerows(CSV_content)
