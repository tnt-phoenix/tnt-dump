#!/usr/bin/env python3

import re
import subprocess
import time

USER_AGENT_HEADER = ("User-Agent: Mozilla/5.0 (compatible; Googlebot/2.1; "
                     "+http://www.google.com/bot.html)")
ACCEPT_HEADER = ("Accept: text/html,application/xhtml+xml,"
                 "application/xml;q=0.9,*/*;q=0.8")
REX = re.compile(r"(?<=id=)[0-9]+$")

with open("url_list.txt") as f:
    # Write file's non-empty lines (trailing spaces removed) to list
    LINES = list(filter(None, (line.rstrip() for line in f)))
for url in LINES:
    time.sleep(1)
    try:
        fn = REX.search(url).group() + ".torrent" if REX.search(url) else url
        subprocess.run(["curl", "--http1.0", "--max-time", "120",
                        "-o", fn, "--retry", "3", "--compressed",
                        "-H", "Accept-Encoding: gzip,deflate,br",
                        "-H", USER_AGENT_HEADER,
                        "-H", "From: googlebot(at)googlebot.com",
                        "-H", ACCEPT_HEADER,
                        "-H", "Cache-Control: no-cache",
                        "-H", "Accept-Language: en-US",
                        "-H", "Host: forum.tntvillage.scambioetico.org",
                        url], check=True)
    except subprocess.CalledProcessError:
        time.sleep(5)
        continue
