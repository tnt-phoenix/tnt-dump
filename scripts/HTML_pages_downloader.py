#!/usr/bin/env python3

import os
import subprocess
import time
from bs4 import BeautifulSoup

POST_URL = "http://www.tntvillage.scambioetico.org/src/releaselist.php"
USER_AGENT_HEADER = ("User-Agent: Mozilla/5.0 (compatible; Googlebot/2.1; "
                     "+http://www.google.com/bot.html)")
ACCEPT_HEADER = ("Accept: text/html,application/xhtml+xml,"
                 "application/xml;q=0.9,*/*;q=0.8")
CURL_ARGS = ["--max-time", "120", "--socks5-hostname", "127.0.0.1:9050",
             "--retry", "3", "--compressed",
             "-H", "Accept-Encoding: gzip,deflate,br",
             "-H", USER_AGENT_HEADER,
             "-H", "From: googlebot(at)googlebot.com",
             "-H", ACCEPT_HEADER, "-H", "Connection: keep-alive",
             "-H", "Cache-Control: no-cache", "-H", "Accept-Language: en-US",
             "-H", "Host: www.tntvillage.scambioetico.org",
             "-H", "Content-Type: application/x-www-form-urlencoded"]

# Download first page to extract total number of pages
subprocess.run(["curl", *CURL_ARGS, "--data", "cat=0&page=1&srcrel=",
                "-o", "tmp.html", POST_URL], check=True)
with open("tmp.html") as fp:
    SOUP = BeautifulSoup(fp, "lxml")
P_NUM = int(SOUP.find("span").get("a"))
ZEROPAD_LEN = len(P_NUM)
os.remove("tmp.html")

for i in range(1, (P_NUM + 1)):
    time.sleep(1)
    try:
        subprocess.run(["curl", *CURL_ARGS, "--data",
                        "cat=0&page={}&srcrel=".format(i), "-o",
                        "page_{}.html".format(str(i).zfill(ZEROPAD_LEN)),
                        POST_URL], check=True)
    except subprocess.CalledProcessError:
        time.sleep(5)
        continue
