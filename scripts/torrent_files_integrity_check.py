#!/usr/bin/env python3

import os
import libtorrent

for fn in os.listdir("."):
    try:
        libtorrent.torrent_info(fn)  # File loading handled by libtorrent
    except RuntimeError as ex:
        print(fn, "->", ex)
