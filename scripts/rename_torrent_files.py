#!/usr/bin/env python3

import os
import libtorrent

for fn in os.listdir("."):
    info = libtorrent.torrent_info(fn)  # File loading handled by libtorrent
    info_hash = str(info.info_hash())  # Convert sha1 type to str
    os.rename(fn, info_hash.upper() + ".torrent")
